all:
	poetry export -f requirements.txt > dist/requirements.txt
	poetry build -f wheel
	docker build -t ririw/udrl:latest .
	docker push ririw/udrl:latest
