FROM python:3.7

ADD dist/requirements.txt /root
RUN pip install -r /root/requirements.txt
ADD dist/upside_down_rl-0.1.0-py3-none-any.whl /root
RUN pip install /root/upside_down_rl-0.1.0-py3-none-any.whl

WORKDIR /root
