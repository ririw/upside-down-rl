import typing as ty

import ignite
import numpy as np
import torch
import torch.utils.data
from grappa import expect
from torch import nn

import upside_down_rl


def _make_dataset(data, state_dim):
    state_size = (len(data), state_dim)
    state_np_tensors = np.zeros(state_size, dtype=np.float32)
    command_np_tensors = np.zeros((len(data), 2), dtype=np.float32)
    idents = np.zeros(len(data), dtype=np.int64)
    act_np_tensors = np.zeros(len(data), dtype=np.int64)
    for i, dat in enumerate(data):
        state_np_tensors[i] = dat.state
        command_np_tensors[i, :] = [dat.delta_r, dat.delta_t]
        act_np_tensors[i] = dat.action
        idents[i] = dat.ident

    state_tensors = torch.tensor(state_np_tensors)
    command_tensors = torch.tensor(command_np_tensors)
    act_tensors = torch.tensor(act_np_tensors)

    is_train = idents % 4 > 0
    is_test = ~is_train

    train_data_set = torch.utils.data.TensorDataset(
        state_tensors[is_train], command_tensors[is_train], act_tensors[is_train]
    )
    test_data_set = torch.utils.data.TensorDataset(
        state_tensors[is_test], command_tensors[is_test], act_tensors[is_test]
    )
    train_data_loader = torch.utils.data.DataLoader(
        train_data_set, batch_size=32, shuffle=True
    )
    test_data_loader = torch.utils.data.DataLoader(
        test_data_set, batch_size=32, shuffle=True
    )
    return train_data_loader, test_data_loader


def score_function(engine):
    val_loss = engine.state.metrics["xentropy"]
    return -val_loss


class ModelUnpacker(nn.Module):
    def __init__(self, module):
        super().__init__()
        self.module = module

    def forward(self, args):
        return self.module(*args)


def pack_args(model_args, *_, **__):
    arg_a, arg_b, arg_c = model_args
    return (arg_a, arg_b), arg_c


class GymModel(upside_down_rl.udrl.ModelInferface):
    def __init__(self, model, state_dim, act_dim, writer=None):
        self.writer = writer
        self.state_dim = state_dim
        self.act_dim = act_dim
        self.model = model
        self.loss = nn.CrossEntropyLoss()
        self.optim = torch.optim.Adam(self.model.parameters())

    def run(self, observation, delta_t: int, delta_r: int):
        raw = self.model(
            torch.tensor(observation, dtype=torch.float32).unsqueeze(0),
            torch.tensor([delta_t, delta_r], dtype=torch.float32).unsqueeze(0),
        )
        probab = torch.softmax(raw, dim=1).detach().numpy().flatten()
        expect(probab.shape).to.equal((self.act_dim,))
        np.testing.assert_almost_equal(probab.sum(), 1.0, decimal=4)
        return np.random.choice(self.act_dim, p=probab)

    def random_action(self):
        return np.random.choice(self.act_dim)

    def train(
        self, data: ty.List[upside_down_rl.udrl.TrainItem]
    ):  # pylint: disable=too-many-locals
        train_data_loader, test_data_loader = _make_dataset(data, self.state_dim)
        trainer = ignite.engine.create_supervised_trainer(
            ModelUnpacker(self.model), self.optim, self.loss, prepare_batch=pack_args
        )

        evaluator = ignite.engine.create_supervised_evaluator(
            ModelUnpacker(self.model),
            metrics={
                "accuracy": ignite.metrics.Accuracy(),
                "xentropy": ignite.metrics.Loss(self.loss),
            },
            prepare_batch=pack_args,
        )

        early_stopper = ignite.handlers.early_stopping.EarlyStopping(
            patience=10, score_function=score_function, trainer=trainer
        )
        evaluator.add_event_handler(ignite.engine.Events.EPOCH_COMPLETED, early_stopper)

        @trainer.on(ignite.engine.Events.EPOCH_COMPLETED)
        def log_validation_results(_):  # pylint: disable=unused-variable
            evaluator.run(test_data_loader)
            metrics = evaluator.state.metrics
            print(
                "Validation Results - Epoch: {} "
                " Avg accuracy: {:.2f}"
                " Avg loss: {:.2f}".format(
                    trainer.state.epoch, metrics["accuracy"], metrics["xentropy"]
                )
            )

        trainer.run(train_data_loader, max_epochs=100)
