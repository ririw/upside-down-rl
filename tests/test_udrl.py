import typing as t

import attr
import gym
import numpy as np
import pytest

import upside_down_rl
from upside_down_rl.udrl import TrainItem


def test_replay_buffer_dumb():
    """Check buffers check their input"""
    with pytest.raises(AssertionError):
        upside_down_rl.udrl.ReplayBuffer(-1)
    with pytest.raises(AssertionError):
        upside_down_rl.udrl.ReplayBuffer(0)


def test_replay_buffer():
    """Check buffers work"""
    buffer = upside_down_rl.udrl.ReplayBuffer(2)
    buffer.add_episode(3)
    assert buffer.episodes() == [3, 3]
    buffer.add_episode(1)
    assert sorted(buffer.episodes()) == [1, 1, 3, 3]
    buffer.add_episode(2)
    assert sorted(buffer.episodes()) == [1, 2, 2, 3]
    buffer.clear()
    assert buffer.episodes() == []


def test_replay_buffer_nlargest():
    buffer = upside_down_rl.udrl.ReplayBuffer(20)
    for i in np.random.permutation(np.arange(20)):
        buffer.add_episode(i)

    nlargest = buffer.nlargest(5)
    assert list(nlargest) == [19, 18, 17, 16, 15]


def test_episode():
    """ Check episode ordering works """
    ep_1 = upside_down_rl.udrl.Episode(
        actions="a", observations="o", rewards="r", total_reward=1, ident=1
    )
    ep_2 = attr.evolve(ep_1, total_reward=2)
    ep_3 = attr.evolve(ep_1, actions="foo")

    assert ep_1 < ep_2
    assert ep_1 <= ep_2
    assert ep_2 > ep_1
    assert ep_2 >= ep_1
    assert ep_2 != ep_1
    assert ep_2 == ep_2  # pylint: disable=comparison-with-itself
    assert ep_3 != ep_2
    assert ep_3 == ep_1


def _env_factory():
    return gym.make("CartPole-v0")


class DummyModel(upside_down_rl.udrl.ModelInferface):
    def train(self, data: t.List[TrainItem]):
        pass

    def run(self, observation, delta_t: int, delta_r: int):
        return self.random_action()

    def random_action(self):
        return np.random.choice(2)


# noinspection PyProtectedMember
def test_randfill():
    """
    Check that the random fill works and that
    it gets some high reward cases
    """
    cfg = upside_down_rl.udrl.URLDConfig(
        replay_buffer_size=10, num_random_plays=1000, model=DummyModel()
    )
    buffer = upside_down_rl.udrl.ReplayBuffer(10)
    upside_down_rl.udrl._random_fill(  # pylint: disable=protected-access
        _env_factory, buffer, cfg
    )
    assert len(buffer.episodes()) == 20
    cfg_small = upside_down_rl.udrl.URLDConfig(
        replay_buffer_size=10, num_random_plays=10, model=DummyModel()
    )
    buffer_small = upside_down_rl.udrl.ReplayBuffer(10)
    upside_down_rl.udrl._random_fill(  # pylint: disable=protected-access
        _env_factory, buffer_small, cfg_small
    )

    mean_large = np.mean([e.total_reward for e in buffer.episodes()])
    mean_small = np.mean([e.total_reward for e in buffer_small.episodes()])
    assert mean_large > mean_small
